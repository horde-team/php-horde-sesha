php-horde-sesha (1.0.0~rc3-9) unstable; urgency=medium

  * Team upload.
  [ Mike Gabriel ]
  * d/watch: Switch to format version 4.

  [ Anton Gladky ]
  * d/salsa-ci.yml: use aptly, simplify.
  * d/control: update standards version to 4.6.2, no changes needed.
  * d/control: fix website protocol
  * d/control: switch BD from pear-horde-channel to pear-channels.
  * d/t/contro: allow-stderr.

 -- Anton Gladky <gladk@debian.org>  Fri, 06 Jan 2023 07:11:48 +0100

php-horde-sesha (1.0.0~rc3-8) unstable; urgency=medium

  * d/patches: Add 1010_phpunit-8.x+9.x.patch. Fix tests with PHPUnit 8.x/9.x.
  * d/t/control: Require php-horde-test (>= 2.6.4+debian0-6~).
  * d/t/control: Require php-horde-db and php-sqlite3.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 03 Oct 2020 10:16:49 +0000

php-horde-sesha (1.0.0~rc3-7) unstable; urgency=medium

  * d/control: Enforce versioned D on php-horde (>= 5.2.23+debian0-2~).
  * d/rules: Move theme files to /etc/horde/.
  * d/maintscript: Turn /usr/share/horde/sesha/themes/ into symlink.

 -- Mike Gabriel <sunweaver@debian.org>  Sat, 04 Jul 2020 00:21:47 +0200

php-horde-sesha (1.0.0~rc3-6) unstable; urgency=medium

  * d/control: Add to Uploaders: Juri Grabowski.
  * d/control: Bump DH compat level to version 13.
  * d/salsa-ci.yml: Add file with salsa-ci.yml and pipeline-jobs.yml calls.
  * d/rules: Drop upstream INSTALL.gz from doc folder.

 -- Mike Gabriel <sunweaver@debian.org>  Wed, 01 Jul 2020 13:55:13 +0200

php-horde-sesha (1.0.0~rc3-5) unstable; urgency=medium

  * Re-upload to Debian. (Closes: #959359).

  * d/control: Add to Uploaders: Mike Gabriel.
  * d/control: Drop from Uploaders: Debian QA Group.
  * d/control: Bump Standards-Version: to 4.5.0. No changes needed.
  * d/control: Add Rules-Requires-Root: field and set it to 'no'.
  * d/copyright: Update copyright attributions.
  * d/upstream/metadata: Add file. Comply with DEP-12.
  * d/tests/control: Stop using deprecated needs-recommends restriction.
  * d/copyright: Update copyright attributions.

 -- Mike Gabriel <sunweaver@debian.org>  Thu, 21 May 2020 10:14:17 +0200

php-horde-sesha (1.0.0~rc3-4) unstable; urgency=medium

  * Bump debhelper from old 11 to 12.
  * d/control: Orphaning package (See #942282)

 -- Mathieu Parent <sathieu@debian.org>  Fri, 18 Oct 2019 20:23:22 +0200

php-horde-sesha (1.0.0~rc3-3) unstable; urgency=medium

  * Update Standards-Version to 4.1.4, no change
  * Update Maintainer field

 -- Mathieu Parent <sathieu@debian.org>  Tue, 15 May 2018 22:08:36 +0200

php-horde-sesha (1.0.0~rc3-2) unstable; urgency=medium

  * Update Standards-Version to 4.1.3, no change
  * Upgrade debhelper to compat 11
  * Update Vcs-* fields
  * Use secure copyright format URI
  * Replace "Priority: extra" by "Priority: optional"

 -- Mathieu Parent <sathieu@debian.org>  Fri, 06 Apr 2018 19:12:32 +0200

php-horde-sesha (1.0.0~rc3-1) unstable; urgency=medium

  * New upstream version 1.0.0~rc3
    - No longer uses Horde_Util::addParameter() (Closes: #829733)
  * Update d/watch to ensure rc is considered newer than beta

 -- Mathieu Parent <sathieu@debian.org>  Wed, 06 Jul 2016 23:14:25 +0200

php-horde-sesha (1.0.0~beta1-13) unstable; urgency=medium

  * Update Standards-Version to 3.9.8, no change
  * Updated d/watch to use https

 -- Mathieu Parent <sathieu@debian.org>  Wed, 08 Jun 2016 20:32:24 +0200

php-horde-sesha (1.0.0~beta1-12) unstable; urgency=medium

  * Update Standards-Version to 3.9.7, no change
  * Use secure Vcs-* fields
  * Rebuild with newer pkg-php-tools for the PHP 7 transition
  * Replace php5-* by php-* in d/tests/control

 -- Mathieu Parent <sathieu@debian.org>  Mon, 14 Mar 2016 10:04:24 +0100

php-horde-sesha (1.0.0~beta1-11) unstable; urgency=medium

  * Upgaded to debhelper compat 9

 -- Mathieu Parent <sathieu@debian.org>  Sat, 24 Oct 2015 11:39:25 +0200

php-horde-sesha (1.0.0~beta1-10) unstable; urgency=medium

  * tests need php-horde-injector

 -- Mathieu Parent <sathieu@debian.org>  Fri, 09 Oct 2015 07:16:41 +0200

php-horde-sesha (1.0.0~beta1-9) unstable; urgency=medium

  * Remove XS-Testsuite header in d/control
  * Update gbp.conf
  * Use override_dh_link instead of dh deprecated --until/--after

 -- Mathieu Parent <sathieu@debian.org>  Mon, 10 Aug 2015 07:41:12 +0200

php-horde-sesha (1.0.0~beta1-8) unstable; urgency=medium

  * Update Standards-Version to 3.9.6, no change

 -- Mathieu Parent <sathieu@debian.org>  Tue, 05 May 2015 07:56:04 +0200

php-horde-sesha (1.0.0~beta1-7) unstable; urgency=medium

  * Fixed DEP-8 tests, by removing "set -x"

 -- Mathieu Parent <sathieu@debian.org>  Sat, 11 Oct 2014 16:06:29 +0200

php-horde-sesha (1.0.0~beta1-6) unstable; urgency=medium

  * Fixed DEP-8 tests

 -- Mathieu Parent <sathieu@debian.org>  Sat, 13 Sep 2014 16:42:10 +0200

php-horde-sesha (1.0.0~beta1-5) unstable; urgency=medium

  * Update Standards-Version, no change
  * Update Vcs-Browser to use cgit instead of gitweb
  * Add dep-8 (automatic as-installed package testing)

 -- Mathieu Parent <sathieu@debian.org>  Tue, 26 Aug 2014 22:16:09 +0200

php-horde-sesha (1.0.0~beta1-4) unstable; urgency=low

  * Use pristine-tar

 -- Mathieu Parent <sathieu@debian.org>  Wed, 05 Jun 2013 13:31:15 +0200

php-horde-sesha (1.0.0~beta1-3) unstable; urgency=low

  * Add B-D on php-horde-role. Fix FTBFS (Closes: #701793)

 -- Mathieu Parent <sathieu@debian.org>  Sun, 07 Apr 2013 15:50:59 +0200

php-horde-sesha (1.0.0~beta1-2) unstable; urgency=low

  * Add a description of Horde in long description
  * Updated Standards-Version to 3.9.4, no changes
  * Replace horde4 by PEAR in git reporitory path
  * Fix Horde Homepage
  * Remove debian/pearrc, not needed with latest php-horde-role

 -- Mathieu Parent <sathieu@debian.org>  Wed, 09 Jan 2013 20:23:49 +0100

php-horde-sesha (1.0.0~beta1-1) unstable; urgency=low

  * New upstream version 1.0.0~beta1

 -- Mathieu Parent <sathieu@debian.org>  Mon, 10 Dec 2012 20:36:01 +0100

php-horde-sesha (1.0.0~alpha1-1) unstable; urgency=low

  * Horde's sesha package
  * Initial packaging (Closes: #695416)

 -- Mathieu Parent <sathieu@debian.org>  Sun, 09 Dec 2012 20:08:29 +0100
